<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="Website von Ready4Stage">
<meta name="author" content="Manuel Wiechert">
<link rel="shortcut icon" href="/Images/favicon.ico" type="image/x-icon">
<link rel="icon" href="/Images/favicon.ico" type="image/x-icon">

<link href="Styles/basic.css" rel="stylesheet">