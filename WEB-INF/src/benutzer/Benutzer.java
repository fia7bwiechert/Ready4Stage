package benutzer;

/**
 * @author <a href="mailto:mwiechert@guskoeln.de">mwiechert</a>
 *
 */
public class Benutzer {

	/** Versions-Tag f�r VSS */
	public static final String SCM_Version = "$Archive: $ $Revision: $ $Date: $ $Modtime: $ $Author: $";

	
	/**
	 * Die ID des Benutzers
	 */
	private int benutzerID;
	/**
	 * Der Vorname des Benutzers
	 */
	private String vorName;
	
	/**
	 * Der Nachname des Benutzers
	 */
	private String nachName;
	
	/**
	 * Der Benutzername des Benutzers
	 */
	private String benutzerName;
	
	/**
	 * Die E-Mail des Benutzers
	 */
	private String benutzerEMail;
	

	/**
	 * Gibt den Benutzer ID des Benutzers zur�ck
	 *
	 * @return benutzerID den Benutzer ID des Benutzers
	 */
	public int getBenutzerID() {
		return this.benutzerID;
	}
	
	/**
	 * Setzt den Benutzer ID des Benutzers
	 * 
	 * @param benutzerID Der Benutzer ID des Benutzers, der gesetzt werden soll
	 */
	public void setBenutzerID(int benutzerID) {
		this.benutzerID = benutzerID;
	}
	/**
	 * Gibt den Vorname des Benutzers zur�ck
	 *
	 * @return vorname den Vorname des Benutzers
	 */
	public String getVorName() {
		return this.vorName;
	}
	
	/**
	 * Setzt den Vorname des Benutzers
	 * 
	 * @param vorName Der Vorname des Benutzers, der gesetzt werden soll
	 */
	public void setVorName(String vorName) {
		this.vorName = vorName;
	}

	/**
	 * Gibt den Nachnamen des Benutzers zur�ck
	 *
	 * @return nachname Den Nachnamen des Benutzers
	 */
	public String getNachName() {
		return this.nachName;
	}
	
	/**
	 * Setzt den Nachname des Benutzers
	 * 
	 * @param nachName Der Nachname des Benutzers, der gesetzt werden soll
	 */
	public void setNachName(String nachName) {
		this.nachName = nachName;
	}
	
	/**
	 * Gibt den Benutzername des Benutzers zur�ck
	 *
	 * @return benutzerName Der Benutzername des Benutzers
	 */
	public String getBenutzerName() {
		return this.benutzerName;
	}
	
	/**
	 * Setzt den Benutzername des Benutzers
	 * 
	 * @param benutzerName Der Benutzername des Benutzers, der gesetzt werden soll
	 */
	public void setBenutzerName(String benutzerName) {
		this.benutzerName = benutzerName;
	}
	
	/**
	 * Gibt die Benutzer E-Mail des Benutzers zur�ck
	 *
	 * @return benutzerEMail Die Benutzer E-Mail des Benutzers
	 */
	public String getBenutzerEMail() {
		return this.benutzerEMail;
	}
	
	/**
	 * Setzt die Benutzer E-Mail des Benutzers
	 * 
	 * @param benutzerEMail Die Benutzer E-Mail des Benutzers, der gesetzt werden soll
	 */
	public void setBenutzerEMail(String benutzerEMail) {
		this.benutzerEMail = benutzerEMail;
	}
	
	/**
	 * Konstrutor des Benutzers
	 *
	 * @param vorName
	 *            Der Vorname des Benutzers
	 * @param nachName
	 *            Der Nachanme des Benutzers
	 * @param benutzerName
	 *            Der Benutzername des Benutzers
	 * @param benutzerEMail
	 *            Die Benutzer E-Mail des Benutzers
	 */
	public Benutzer(String vorName, String nachName, String benutzerName, String benutzerEMail) {
		this.vorName = vorName;
		this.nachName = nachName;
		this.benutzerName = benutzerName;
		this.benutzerEMail = benutzerEMail;
	}
}
