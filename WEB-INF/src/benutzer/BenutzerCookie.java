package benutzer;

/**
 * @author <a href="mailto:mwiechert@guskoeln.de">mwiechert</a>
 *
 */
public class BenutzerCookie {

	/** Versions-Tag f�r VSS */
	public static final String SCM_Version = "$Archive: $ $Revision: $ $Date: $ $Modtime: $ $Author: $";
	
	private Benutzer benutzer = null;

	/**
	 * Gibt den Benutzer zur�ck
	 * 
	 * @return benutzer Der Benutzer des Cookies
	 */
	public Benutzer getBenutzer() {
		return benutzer;
	}
	
	/**
	 * Setzt den Benutzer
	 * 
	 * @param benutzer Der Benutzer, der gesetzt werden soll
	 */
	public void setBenutzer(Benutzer benutzer) {
		this.benutzer = benutzer;
	}
	
}