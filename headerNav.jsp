<header>
	<h1><a tabindex="-1" title="Startseite" href="index.html">Ready 4 Stage</a></h1>
	<p>F�r <span class="akzentfarbe1">alle</span>, die <span class="akzentfarbe2">Musik</span> lieben.</p>
</header>

<nav>
	<ul>
		<li><% if (aktuelleSeite == 1) { %> <a  aria-current="page"> <% } else { %> <a href="template.jsp"> <% } %>Startseite</a></li>
		<li><% if (aktuelleSeite == 2) { %> <a  aria-current="page"> <% } else { %> <a href=".jsp"> <% } %>Aufnahmeantrag</a></li>
		<li><% if (aktuelleSeite == 3) { %> <a  aria-current="page"> <% } else { %> <a href=".jsp"> <% } %>Kurse</a></li>
		<li><% if (aktuelleSeite == 4) { %> <a  aria-current="page"> <% } else { %> <a href=".jsp"> <% } %>Preise</a></li>
		<li><% if (aktuelleSeite == 5) { %> <a  aria-current="page"> <% } else { %> <a href=".jsp"> <% } %>Stundenplan</a></li>
		<li><% if (aktuelleSeite == 6) { %> <a  aria-current="page"> <% } else { %> <a href=".jsp"> <% } %>Anmelden</a></li>
	</ul>
</nav>